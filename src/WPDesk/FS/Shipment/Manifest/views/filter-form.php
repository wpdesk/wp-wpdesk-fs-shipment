<?php
/**
 *
 */
?><div class="alignleft actions">
    <select name="flexible_shipping_integration_filter">
        <option value=""><?php _e( 'All manifests', 'wp-wpdesk-fs-shipment' ); ?></option>
        <optgroup label="<?php _e( 'Integration', 'wp-wpdesk-fs-shipment' ); ?>">
            <?php foreach ( $integrations as $key => $val ) : ?>
                <option value="<?php echo $key; ?>" <?php echo ($key == $integration ? 'selected' : '' );  ?>><?php echo $val; ?></option>
            <?php endforeach; ?>
        </optgroup>
    </select>
</div>
