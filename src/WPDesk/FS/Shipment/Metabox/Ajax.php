<?php

namespace WPDesk\FS\Shipment\Metabox;

use WPDesk\PluginBuilder\Plugin\Hookable;

class Ajax implements Hookable {

    public function hooks() {
        add_action( 'wp_ajax_flexible_shipping', array( $this, 'wp_ajax_flexible_shipping' ) );
    }

    public function wp_ajax_flexible_shipping() {
        $json = array('status' => 'fail');
        $json['message'] = __( 'Unknown error!', 'wp-wpdesk-fs-shipment' );
        if ( empty( $_REQUEST['nonce'] ) || !wp_verify_nonce( sanitize_text_field( $_REQUEST['nonce'] ), 'flexible_shipping_shipment_nonce' ) ) {
            $json['status'] = 'fail';
            $json['message'] = __( 'Nonce verification error! Invalid request.', 'wp-wpdesk-fs-shipment' );
        }
        else if ( empty( $_REQUEST['shipment_id'] ) ) {
            $json['status'] = 'fail';
            $json['message'] = __( 'No shipment id!', 'wp-wpdesk-fs-shipment' );
        }
        else if ( empty( $_REQUEST['data'] ) || !is_array( $_REQUEST['data'] ) ) {
            $json['status'] = 'fail';
            $json['message'] = __( 'No data!', 'wp-wpdesk-fs-shipment' );
        }
        else {
            $shipment = fs_get_shipment( intval( $_REQUEST['shipment_id'] ) );
            $action = sanitize_key( $_REQUEST['fs_action'] );
            $data = $_REQUEST['data'];
            try {
                $ajax_request = $shipment->ajax_request( $action, $data );
                if ( is_array( $ajax_request ) ) {
                    $json['content'] = $ajax_request['content'];
                    $json['message'] = '';
                    if ( isset( $ajax_request['message'] ) ) {
                        $json['message'] = $ajax_request['message'];
                    }
                }
                else {
                    $json['content'] = $ajax_request;
                    $json['message'] = '';
                    if ( $action == 'save' ) {
                        $json['message'] = __( 'Saved', 'wp-wpdesk-fs-shipment' );
                    }
                    if ( $action == 'send' ) {
	                    $json['message'] = __( 'Created', 'wp-wpdesk-fs-shipment' );
                    }
                }
                $json['status'] = 'success';
                if ( ! empty( $ajax_request['status'] ) ) {
                    $json['status'] = $ajax_request['status'];
                }
            }
            catch ( \Exception $e ) {
                $json['status'] = 'fail';
                $json['message'] = $e->getMessage();
            }
        }
        echo json_encode($json);
        die;
    }

}

