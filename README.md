[![pipeline status](https://gitlab.com/wpdesk/wp-wpdesk-fs-shipment/badges/master/pipeline.svg)](https://gitlab.com/wpdesk/wp-wpdesk-fs-shipment/commits/master) 
Integration: [![coverage report](https://gitlab.com/wpdesk/wp-wpdesk-fs-shipment/badges/master/coverage.svg?job=integration+test+lastest+coverage)](https://gitlab.com/wpdesk/wp-wpdesk-fs-shipment/commits/master)
Unit: [![coverage report](https://gitlab.com/wpdesk/wp-wpdesk-fs-shipment/badges/master/coverage.svg?job=unit+test+lastest+coverage)](https://gitlab.com/wpdesk/wp-wpdesk-fs-shipment/commits/master)

wp-wpdesk-fs-shipment
====================
