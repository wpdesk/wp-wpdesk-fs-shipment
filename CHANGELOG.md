## [2.0.11] - 2022-05-24
### Fixed
- libraries

## [2.0.10] - 2021-10-08
### Fixed
- Exception namespace

## [2.0.9] - 2021-10-05
### Fixed
- Integration metabox CSS

## [2.0.8] - 2021-09-29
### Fixed
- Allowed wp-plugin-flow v 3

## [2.0.7] - 2021-08-09
### Fixed
- WooCommerce Subscriptions integration for InPost

## [2.0.6] - 2021-07-21
### Fixed
- WooCommerce Subscriptions integration

## [2.0.5] - 2021-07-19
### Removed
- Note when shipment has been created

## [2.0.4] - 2021-07-12
### Fixed
- automatic label download

## [2.0.3] - 2021-07-09
### Added
- throws Exception for labels

## [2.0.2] - 2021-07-01
### Fixed
- prefixer hack for static method
- added missing css

## [2.0.1] - 2021-07-01
### Fixed
- Fatal error on old Flexible Shipping version

## [2.0.0] - 2021-06-28
### Added
- second attempt to library

## [1.0.4] - 2019-08-05
### Changed
- Fixed Fatal error: boolean given in mutex, #PB-1241

## [1.0.3] - 2019-07-08
### Changed
- Added close counter to rate notice

## [1.0.2] - 2019-07-03
### Changed
- Added rate notice with variants

## [1.0.1] - 2019-06-26
### Changed
- Moved html-order-add_shipping-metabox.php here

## [1.0.0] - 2019-06-17
### Added
- Init